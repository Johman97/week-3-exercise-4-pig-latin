/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pig.latin;

/**
 *
 * @author calebedwards
 */
import java.util.Scanner;

public class PigLatin {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String userInput = "";
        String[] tokens;

        do {
            System.out.println("Enter word or sentence: ");
            userInput = input.nextLine();
            tokens = userInput.split(" ");

            String newSentence = "";

            for (String token : tokens) {
                if (token.startsWith("a") || token.startsWith("e") || token.startsWith("i")
                        || token.startsWith("o") || token.startsWith("u")
                        || token.startsWith("A") || token.startsWith("E") || token.startsWith("I")
                        || token.startsWith("O") || token.startsWith("U")) {
                    newSentence += token + "way ";
                } else {
                    newSentence += token.substring(1) + token.substring(0, 1) + "ay ";
                }
            }

            System.out.println(newSentence);
            System.out.println(newSentence.toUpperCase());

        } while (!userInput.equals("quit"));
    }
}
